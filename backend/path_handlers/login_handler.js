// import bcrypt
import bcrypt from "bcrypt";

// importing jwt_token_genrator
import { jwt_token_generator } from "../utils/jwt_token.js";

// importing Users model
import { Users
} from "../utils/model.js";

/**
 * This function is used to verify whether the user exists or not.
 * If the user exists then checks the log_in params for login purpose
 * @param {*} req email,password from body which are used for user login
 * @param {*} res sends statuscode and status_message as response for successfull login '200'
 * @param {*} next it is an express middleware
 *
 * if the employee doesnt found by employee id 
 * sends status code-404 and status_messege as error in finding user by email as response
 * if password matches
 * if jwt created
 * sends status code-200 and jwt token  as response
 *
 * if jwt not created
 * sends status code-400 and login unsuccesfull as response
 *
 * if password doesnot matches
 * sends status code-404 and password doesnt match as response
 *
 *
 * if the user doesnot exists
 * sends status code-404 and email doesnt exist as response
 *
 */
async function login_handler(req, res) {
  // accessing email and pasword from body
  const { empid, password } = req.body;
  // const user_login_data = {empid,password,jwt_token}

  // Checking whether the employee email exists or not
  Users.find({ empid: empid }, (err, data) => {
    if (err) {
      res
        .status(406)
        .send({ status_message: "Error in finding user by employee ID" });
    } else {
      // if user exists checks for password
      if (data.length !== 0) {

        // const validate_password = bcrypt.compare(password, data[0].password);
        // if password matches calls the jwt_token_generator()
        if (password===data[0].password) {
          const empid = data[0].empid;
          const role = data[0].role;
          const jwt_token = jwt_token_generator(empid, role);

          // if jwt_token is generated then login successful
          if (jwt_token !== "undefined") {
            res.status(200).send({ jwt_token: jwt_token, role: role, status:"login sucessful"});
          }
          // if jwt_token is not generated then sends response as login unsuccessful
          else {
            res.status(400).send({ status: "log in unsuccessful" });
          }
        }
        // if the password doesn't matches sends 404 as response
        else {
          res.status(404).send({ status: "invalid password" });
        }
      }
      // if user doesn't exists sends 404 as response
      else {
        res.status(404).send({ status: "Employee ID doesn't exists" });
      }
    }
  });
}

export { login_handler };
