import { Users } from "../Utils/model.js";

/**
 * This function is used to check whether the user is registered successfully or not
 * If the user is registered successfully then it will send the status_message
 * @param {*} req body params from body which are used for user to register
 * @param {*} res sends statuscode and status_message as response for successfull registeration '201'
 * @param {*} next it is an express middleware
 */
function register_handler(req, res) {
  //
  const { empid, name, password, email, role, phoneNo, dateofjoining, security_question, security_answer, new_password} =
    req.body;

  const user_data = {
    empid,
    name,
    password,
    email,
    role,
    phoneNo,
    dateofjoining,
    security_question,
    security_answer,
    new_password
  };

  const user = new Users(user_data);

  Users.find(
    {
      $or: [ { email: email },{ empid: empid }]},
    (err, data) => {
      if (err) {
        return res
          .status(406)
          .send({ message: "Error in finding user by employee id" });
      }
      if(data.length!==0){
           if (data[0].email === email) {
             return res
               .status(402)
               .send(JSON.stringify({ message: "Email ID already exists" }));
      }}
      if (data.length == 0) {
        user.save().then(() => {
         return  res.status(201).send({ message: "User Registration Successfull" });
        });
      } else {
        return res.status(409).send({ message: "employee ID already exists" });
      }
    }
  );

}

export { register_handler };
