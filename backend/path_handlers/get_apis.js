import { Users } from "../utils/model.js";

const get_employee_details_handler = (request, response) => {
  // const jwt_token = request.headers.authorization.split(" ")[1];

  const { empid } = request.empid;
  Users.findOne({ empid: request.empid }, (err, data) => {
    if (err) {
      response
        .status(404)
        .send({ error: "empid " + empid + " is not there in db" });
    } else {
      response.send(data);
    }
  });
};

const get_time_sheets_handler = (request, response) => {
  Users.findOne({ empid: request.empid }, (err, data) => {
    if (err) {
      response.status(404).send({ error: "empid" + empid + "not found" });
    } else {
      response.send(data.time_sheets);
    }
  });
};

const get_daily_tasks_handler = (request, response) => {
  const { date } = request.params;
  Users.findOne({ empid: request.empid }, (err, data) => {
    if (err) {
      response.status(404).send({ error: "empid" + empid + "not found" });
    } else {
      data.time_sheets.map((item) => {
        if (item.date == date) {
          response.send(item);
        }
      });
    }
  });
};

export {
  get_employee_details_handler,
  get_time_sheets_handler,
  get_daily_tasks_handler,
};
