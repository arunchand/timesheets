import {Users,daily_tasks} from "../utils/model.js"

function task_handler(req, res, next) {
  const { date, task_name,hours, description } =
    req.body;
  const { empid } = req.empid;
  const time_sheet_data = {
    date,
    task_name,
    hours,
    description,
  };
 console.log("empid",req.empid)
//   const user_time_sheet_data = new daily_tasks(time_sheet_data);
//      Users.save().then(() => {
//     res.status(201).send({ message: "Timesheet Updated successfully" });
//   });
//   Users.time_sheets.push(time_sheet_data)
//   Users.save().then(()=>{
//       res.status(201).send({message:"Task uploaded"})
//   })
Users.updateOne({empid:req.empid},{$push:{time_sheets:time_sheet_data}}).then(()=>{
        res.send({message : "Task Updated"})
    })
}

export {task_handler}
