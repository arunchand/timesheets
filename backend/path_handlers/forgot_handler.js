//importing model
import { Users } from "../utils/model.js";

function forgot_password_handler(req, res, next) {
  const { empid, security_question, security_answer, new_password } = req.body;

  Users.findOne({ empid: empid }, (err, data) => {
    if (err) {
      res.status(404).send({ message: "Error in finding user by empid" });
    } else {
      if (data) {
        if ((data.security_answer === security_answer) && (data.security_question === security_question)){
          Users.updateOne(
            { empid: empid },
            { $set: { password: new_password } }
          )
            .then(() => {
              res
                .status(200)
                .send({ message: "Password updated successfully" });
            })
            .catch(() => {
              res.status(304).send({ message: "Unable to update password" });
            });
        } else {
          res.status(401).send({ message: "security answer is incorrect" });
        }
      } else {
        res.status(404).send({ message: "User doesn't exist" });
      }
    }
  });
}

export { forgot_password_handler };
