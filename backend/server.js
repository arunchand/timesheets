import bodyParser from "body-parser";
import express from "express";
import cors from "cors";

import { register_keys_validator } from "./helpers/helpers.js"
import { forgot_password_handler } from "./path_handlers/forgot_handler.js";
import { get_daily_tasks_handler, get_employee_details_handler,get_time_sheets_handler } from "./path_handlers/get_apis.js";
import { login_handler } from "./path_handlers/login_handler.js";
import { register_handler } from "./path_handlers/register_handler.js";
import { forgotpassword_data_validator } from "./utils/forgot_validator.js";
import { jwt_validator } from "./utils/jwt_token.js";
import { login_data_validator } from './utils/login_validator.js';
import { register_data_validator } from './utils/register_validator.js';
import { forgot_keys_validator } from "./utils/forgot_key_validator.js";
import {task_key_validator} from "./utils/task_key_validator.js";
import { task_params_validator } from "./utils/task_params_validator.js";
import { task_handler } from "./path_handlers/task_handler.js";
import { login_keys_validator } from "./utils/login_keys_validator.js";
import { admin_jwt_validator, role_check_hr } from "./utils/role_checkers.js";
import { update_status_handler } from "./path_handlers/update_status.js";
import { update_params_validator } from "./utils/update_params_validator.js";
import { update_keys_validator } from "./utils/update_key_validator.js";
// creating an instance of express
const app = express();
// Returns middleware that only parses json
app.use(bodyParser.json());
// Returns middleware that only parses urlencoded bodies
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cors({ origin: "*" }));

//--------------------------------POST CALLS-------------------------------------------------------
// api call for register
app.post("/register", admin_jwt_validator,register_keys_validator, register_data_validator, register_handler )

// api call for login
app.post("/login", login_keys_validator,login_data_validator, login_handler)

// api call for forgot
app.post("/forgot",forgot_keys_validator, forgotpassword_data_validator,forgot_password_handler)

//api call to post the daily taks
app.post("/daily_task",jwt_validator,task_key_validator,task_params_validator,task_handler)

//-----------------------------GET CALLS-------------------------------------------------------------------------

//getting employee details
app.get("/get/Employee_details",jwt_validator,get_employee_details_handler)

//getting the time sheets 
app.get("/get/time_sheets",jwt_validator,get_time_sheets_handler)

//getting the daily task
app.get("get/daily_task",jwt_validator,get_daily_tasks_handler)


//-----------------------------------PUT CALLS--------------------------------------------------------------------
app.put("/update_status",admin_jwt_validator,update_keys_validator,update_params_validator, update_status_handler);

//listnening to server
app.listen(8002,()=>
{
   console.log("The server is running at 8002")
})