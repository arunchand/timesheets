function task_key_validator(req, res, next) {
  const keys = req.body;
  const keylist = [];
  for (const [key, value] of Object.entries(keys)) {
    if (
      !(
        key.toLowerCase() == "date" ||
        key.toLowerCase() == "task_name" ||
        key.toLowerCase() == "hours" ||
        key.toLowerCase() == "description"
      )
    ) {
      return res
        .status(406)
        .send(`Please provide the exact key. The key:${key} is incorrect`);
    }
    if (keylist.includes(key.toLowerCase())) {
      return res.status(406).send(`key naming:${key} is reapeting`);
    }
    keylist.push(key.toLowerCase());
  }
  if (keylist.length < 4) {
    return res.status(406).send(`${4 - keylist.length} key missing`);
  }
  if (keylist.length > 4) {
    return res.status(406).send(`${keylist.length - 4} key extra`);
  }
  next();
}

export { task_key_validator };
