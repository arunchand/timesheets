import jwt from "jsonwebtoken";

const MYSECRET = "123456789N";

/**
 * jwt_token_generator() is used for generating jwt_token when the user logins
 * @param {*} empid which is used as part of payload
 * @param {*} role which is used as a part of the payload
 * @returns jwt_token which is generated
 */
function jwt_token_generator(empid, role) {
  // accepting emp_id as params and storing it in payload as object
  var payload = {
    empid: empid,
    role: role,
  };

  // generates jwt token
  const jwt_Token = jwt.sign(payload, "MY_SECRET_TOKEN");
  return jwt_Token;
}

// function jwt_validator(req, res, next) {
//   const query_token = req.headers.authorization.split(" ")[1];

//   if (query_token) {
//     jwt.verify(query_token, MYSECRET, (err, user) => {
//       if (err) {
//         // 403 Forbidden Access Denied – You don’t have permission to access
//         return res.status(403).send({ status: "JWT token is not valid" });
//       }
//       next();
//     });
//   } else {
//     //  * 401 Unauthorized - shows when there is no access  
//     res.sendStatus(401);
//   }
// }


// export { jwt_token_generator,jwt_validator };


const jwt_validator = (req, res, next) => {
  const { authorization } = req.headers;
  if (!authorization) {
    console.log("Authorization header not found in request object");
    res.status(401).json({ error: "you must be logged in" });
  }
  const token = authorization.split(" ")[1];
  jwt.verify(token, "MY_SECRET_TOKEN", (err, payload) => {
    if (err) {
      return res.status(401).json({ error: "you must be logged inside" });
    } else {
      req.empid = payload.empid;
      req.role = payload.role;
      next();
    }
  });
};
  
  
  
  export  {jwt_token_generator,jwt_validator}
