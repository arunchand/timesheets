import { _empid_helper, _password_helper } from "../helpers/helpers.js";

/**
 * This is a helper middleware function to help with validation of login
 * parameters.
 * @param {}  req   HTTP req object as recieved by Express backend
 * @param {*} res   HTTP response object to be populated by Express backend
 * @param {*} next  next() is a function to be called if this middlware validation is success
 * @returns
 */
const login_data_validator = (req, res, next) => {
  const { empid, password } = req.body;
  const error_object = {};

  error_object.empid = _empid_helper(empid).empid;
  error_object.password = _password_helper(password).password;

  // error_object.security_question =
  //   _security_question_helper(security_question).security_question;
  // error_object.password = _password_helper(password).password;
for (const [key, value] of Object.entries(error_object)) {
  if (value) {
    return res.status(200).send(error_object);
  }
}
  // if (Object.getOwnPropertyNames(error_object).length > 0) {
  //   res.status(409).send(error_object);
  // }

  next();
};

export { login_data_validator };
