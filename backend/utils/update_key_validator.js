function update_keys_validator(req, res, next) {
  const keys = req.body;
  const keylist = [];
  for (const [key, value] of Object.entries(keys)) {
    if (
      !(
        key.toLowerCase() == "empid" ||
        key.toLowerCase() == "employee_status" 
      )
    ) {
      return res
        .status(406)
        .send(`Please provide the exact key empid or employee_status. The key:${key} is incorrect`);
    }
    if (keylist.includes(key.toLowerCase())) {
      return res.status(406).send(`key naming:${key} is reapeting`);
    }
    keylist.push(key.toLowerCase());
  }
  if (keylist.length < 2) {
    return res.status(406).send(`${2 - keylist.length} key missing`);
  }
  if (keylist.length > 2) {
    return res.status(406).send(`${keylist.length - 2} key extra`);
  }
  next();
}

export { update_keys_validator };
