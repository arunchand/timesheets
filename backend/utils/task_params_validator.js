 import { _date_helper,_task_name_helper,_hours_helper,_description_helper } from "../helpers/helpers.js";
 
 
 const task_params_validator = (req, res, next) => {
  const {
    date,
    task_name,
    hours,
    description
  } = req.body;
  const error_object = {};

  error_object.date = _date_helper(date).date;
  error_object.task_name = _task_name_helper(task_name).task_name;
  error_object.hours = _hours_helper(hours).hours;
  error_object.description = _description_helper(description).description;

 for (const [key, value] of Object.entries(error_object)) {
   if (value) {
     return res.status(409).send(error_object);
   }
 }
 next();
}

export{task_params_validator}