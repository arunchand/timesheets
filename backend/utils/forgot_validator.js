import {
  _empid_helper,
  _new_password_helper,
  _security_answer_helper,
  _security_question_helper,
} from "../helpers/helpers.js";

/**
 * This is a helper middleware function to help with validation of forgot password
 * parameters.
 * @param {}  req   HTTP req object as recieved by Express backend
 * @param {*} res   HTTP response object to be populated by Express backend
 * @param {*} next  next() is a function to be called if this middlware validation is success
 * @returns
 */
const forgotpassword_data_validator = (req, res, next) => {
  const { empid, security_question, security_answer, new_password } = req.body;

  const error_object = {};
  error_object.empid = _empid_helper(empid).empid;
  error_object.security_question =
    _security_question_helper(security_question).security_question;
  error_object.security_answer =
    _security_answer_helper(security_answer).security_answer;
  error_object.new_password = _new_password_helper(new_password).new_password;
 
  for(const [key,value] of Object.entries(error_object)){
    if(value){
      return res.status(409).send(error_object);
    }
  }
  next();
};

export { forgotpassword_data_validator };
