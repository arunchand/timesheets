import jwt from "jsonwebtoken";

/**
 *
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */

const admin_jwt_validator = (req, res, next) => {
  const { authorization } = req.headers;
  if (!authorization) {
    res.status(401).json({ error: "you must be logged in as Admin" });
  }
  const token = authorization.split(" ")[1];
  jwt.verify(token, "MY_SECRET_TOKEN", (err, payload) => {
    if (err) {
      return res.status(401).json({ error: "you must be logged inside" });
    } else {
    //   req.empid = payload.empid;
    //   req.role = payload.role;
      if (payload.role.toLowerCase() == "admin") {
        next();
      } else {
        res.status(401).send({
          status_message:
            "You must be an admin to use this feature not a " + payload.role,
        });
      }
    }
  });
};

/**
 *
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
function role_check_hr(req, res, next) {
  const { role } = req;

  if (role == "HR") {
    next();
  } else {
    res.status(401).send({
      status_message: "You must be an HR to use this feature not a " + role,
    });
  }
}

/**
 *
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
function role_check_employee(req, res, next) {
  const { role } = req;

  if (role == "Employee") {
    next();
  } else {
    res.status(401).send({
      status_message:
        "You must be an employee to use this feature not a " + role,
    });
  }
}

export { admin_jwt_validator, role_check_employee, role_check_hr };
