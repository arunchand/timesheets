// importing mongoose from mongoose
import mongoose from "mongoose";


/**
 * daily tasks are the sub schema of the timesheets 
 */

const daily_tasks_schema = mongoose.Schema({
  date:          { type: Date },
  task_name:     {type:String,required:true},
  hours:         {type:Number,required:true},
  description:   {type: String}
});
/**
 * This is the definition of the schema to be used to convert into a model
 * for the MongoDB implementation.
 * This model represents a User on the application.
 */
const UserData = new mongoose.Schema({
  name: { type: String, required: true },
  empid: { type: Number, required: true, unique: true },
  password: { type: String, required: true },
  dateofjoining: { type: Number, required: true },
  role: { type: String, required: true, default: "Employee" },
  email: { type: String, required: true},
  phoneNo: { type: Number, required: true },
  security_question: { type: String, required: true },
  security_answer: { type: String, required: true },
  time_sheets: [Object],
  empstatus:{type:String,default:"Active"}
});



export { UserData,daily_tasks_schema}