
import {
  _address_helper,
  _date_of_joining_helper,
  _email_helper,
  _empid_helper,
  _name_helper,
  _new_password_helper,
  _phoneNo_helper,
  _role_helper,
  _security_answer_helper,
  _security_question_helper,
  //register_keys_validator,
} from "../helpers/helpers.js";

/**
 * This is a helper middleware function to help with validation of register
 * parameters.
 * @param {}  req   HTTP req object as recieved by Express backend
 * @param {*} res   HTTP response object to be populated by Express backend
 * @param {*} next  next() is a function to be called if this middlware validation is success
 * @returns
 */
const register_data_validator = (req, res, next) => {
  const { name, empid, dateofjoining, email, phoneNo, role, security_question, security_answer, new_password} = req.body;
  //  register_keys_validator(req.body,response)
  const error_object = {};
  error_object.name = _name_helper(name).name;
  error_object.email = _email_helper(email).email;
  error_object.empid = _empid_helper(empid).empid;
  error_object.dateofjoining = _date_of_joining_helper(dateofjoining).dateofjoining;
  error_object.phoneNo = _phoneNo_helper(phoneNo).phoneNo;
  error_object.role = _role_helper(role).role;
  //error_object.address = _address_helper(address).address;
  error_object.security_question = _security_question_helper(security_question).security_question;
  //console.log(`error obj ${error_object}`);
  error_object.security_answer = _security_answer_helper(security_answer).security_answer;
  // error_object.new_password=_new_password_helper(new_password).new_password;
  //console.log(Object.getOwnPropertyNames(error_object).length)
  for(const [key,value] of Object.entries(error_object)){
    if(value){
      return res.status(406).send(error_object);
    }
  }
  // if (Object.getOwnPropertyNames(error_object).length > 0) {
  //   res.status(200).send(error_object);
  // }

  next();
};

export { register_data_validator };
