import { _empid_helper,_empstatus_helper } from "../helpers/helpers.js";



const update_params_validator = (req, res, next) => {
  const { empid, empstatus } = req.body;

  const error_object = {};
  error_object.empid = _empid_helper(empid).empid;
  error_object.empstatus = _empstatus_helper(empstatus).empstatus;

  for (const [key, value] of Object.entries(error_object)) {
    if (value) {
      return res.status(409).send(error_object);
    }
  }
  next();
};

export { update_params_validator };
