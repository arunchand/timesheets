import React from "react";
import { Link } from "react-router-dom";
import "./empList.css";

const EmpListAdmin = () => {
  return (
    <div class="container">
      <div class="emplistadmin-header">
        List of all Employees
        <Link to="/addemp">
          <button class="add-emp-btn">+</button>
        </Link>
      </div>
      <div class="list">
        <div class="heading">
          <h4>SL.no</h4>
          <h4>Employee Name</h4>
          <h4>Employee Id</h4>
          <h4>Delete Employee</h4>
        </div>
        <div class="data">
          <h4>1</h4>
          <h4>Arun</h4>
          <h4>10789</h4>
          <button class="del-btn">Delete</button>
        </div>
        <div class="data">
          <h4>2</h4>
          <h4>Jameel</h4>
          <h4>10766</h4>
          <button class="del-btn">Delete</button>
        </div>
        <div class="data">
          <h4>3</h4>
          <h4>Hari</h4>
          <h4>10754</h4>
          <button class="del-btn">Delete</button>
        </div>
        <div class="data">
          <h4>3</h4>
          <h4>Hari</h4>
          <h4>10754</h4>
          <button class="del-btn">Delete</button>
        </div>
        <div class="data">
          <h4>3</h4>
          <h4>Hari</h4>
          <h4>10754</h4>
          <button class="del-btn">Delete</button>
        </div>
        <div class="data">
          <h4>3</h4>
          <h4>Hari</h4>
          <h4>10754</h4>
          <button class="del-btn">Delete</button>
        </div>
      </div>
    </div>
  );
};

export default EmpListAdmin;
