import React from "react";
import "./empList.css";
import { Link } from "react-router-dom";

const EmpListHr = () => {
  return (
    <div class="container">
      <div class="emplisthr-header">List of all Employees</div>
      <div class="emp-list">
        <div class="heading">
          <h4>SL.no</h4>
          <h4>Employee Name</h4>
          <h4>Employee Id</h4>
        </div>
        <div class="data">
          <h4>1</h4>
          <h4>Arun</h4>
          <h4>10789</h4>
        </div>
        <div class="data">
          <h4>2</h4>
          <h4>Jameel</h4>
          <h4>10766</h4>
        </div>
        <div class="data">
          <h4>3</h4>
          <h4>Saishree</h4>
          <h4>10754</h4>
        </div>
        <div class="data">
          <h4>3</h4>
          <h4>Hari</h4>
          <h4>10754</h4>
        </div>
        <div class="data">
          <h4>3</h4>
          <h4>Hari</h4>
          <h4>10754</h4>
        </div>
        <div class="data">
          <h4>3</h4>
          <h4>Hari</h4>
          <h4>10754</h4>
        </div>
      </div>
    </div>
  );
};

export default EmpListHr;
