import React, { Component } from 'react'
import { Link,Redirect } from 'react-router-dom';
import Cookies from 'js-cookie';
import TimeSheetContext from '../../Context/TimesheetContext';
import './index.css';

const HR = () => {
    const jwtToken = Cookies.get('jwt_token')
    if (jwtToken !== undefined) {
      return <Redirect to="/" />
    }
    return(
      <TimeSheetContext>
        {
          (value)=>{
            console.log(value)
            return(
              <div className="form-row">
      <div className='login-form-container card2-rep'>
        {/* <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcROAnJMOErrPA2sLq03EtX1jRciFcOyGeRgL8YGsCfPoGnYYwHmglvI-E2KmWrwbaNONJI&usqp=CAU" class="logo1" alt='website'/> */}
        </div>
      <div className="card1-rep">
  <div className="panel-manager-login position-relative">
    <div className=" py-4 px-4 rounded-top border-bottom text-center text-white">
      <h1 className="heading">Hr Login</h1>
      
    </div>
    <form className="p-4">
      <div className="form-group">
        <label htmlFor="exampleInputEmail1"></label>
        <input
          type="empid"
          className="form-control form-control-lg"
          id="exampleInputEmail1"
          aria-describedby="emailHelp"
          placeholder="Enter empid"
        />
      </div>
      <div className="form-group">
        <label htmlFor="exampleInputPassword1"></label>
        <input
          type="password"
          className="form-control form-control-lg"
          id="exampleInputPassword1"
          placeholder="Password"
        />
      </div>
      <div className="row">
        <div className="col-6">
          <div className="form-group form-check" style={{ display: "none" }}>
            <input
              type="checkbox"
              className="form-check-input"
              id="exampleCheck1"
            />
            <label className="form-check-label" htmlFor="exampleCheck1">
              Check me out
            </label>
          </div>
        </div>
        <div className="col-6 text-right">
          <button type="button" className="reset-password-btn btn btn-link heading p-0">
            Forgot Password?
          </button>
        </div>
      </div>
      <div className="d-block text-center">
        <button
          type="button"
          className="button1 badge-pill px-4"
        >
          Login
        </button>
      </div>
    </form>
  </div>
</div>
              </div>
            )
          }
        }
      
</TimeSheetContext>

    )
  }

export default HR
