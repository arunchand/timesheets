import React, { useState } from "react";
import "./index.css";
import { Link, useHistory } from "react-router-dom";
import {
  _empid_validator,
  _new_password_validator,
  _security_answer_validator,
  _security_question_validator,
} from "../../input_validators";

const ForgotPassword = () => {
  const [empid, setEmpid] = useState("");
  const [security_question, setSecurityQuestion] = useState("");
  const [security_answer, setSecurityAnswer] = useState("");
  const [new_password, setNewPassword] = useState("");
  const [empidError, setEmpidError] = useState("");
  const [newpasswordError, setNewPasswordError] = useState("");
  const [security_question_err, setQuestionError] = useState("");
  const [security_answer_err, setAnswerError] = useState("");
  const [headstatus, setHeadstatus]=useState("")

  const history = useHistory();
  const onChangeEmpid = (event) => {
    setEmpid(parseInt(event.target.value));
    setEmpidError("");
  };
  const onBlurEmpid = (event) => {
    validateEmpid(event.target.value);
    //setNewPasswordError("")
  };
  const validateEmpid = (empid) => {
    setEmpidError(_empid_validator(empid));
  };

  const onChangeNewPassword = (event) => {
    setNewPassword(event.target.value);
    setNewPasswordError("");
  };
  const onBlurNewPassword = (event) => {
    validateNewPassword(event.target.value);
  };
  const validateNewPassword = (new_password) => {
    setNewPasswordError(_new_password_validator(new_password));
  };

  const onChangeSecurityQuestion = (event) => {
    setSecurityQuestion(event.target.value);
    setQuestionError("");
  };
  const onBlurSecurityQuestion = (event) => {
    validateSecurityQuestion(event.target.value);
  };
  const validateSecurityQuestion = (security_question) => {
    setQuestionError(_security_question_validator(security_question));
  };

  const onChangeSecurityAnswer = (event) => {
    setSecurityAnswer(event.target.value);
    setAnswerError("");
  };
  const onBlurSecurityAnswer = (event) => {
    validateSecurityAnswer(event.target.value);
  };
  const validateSecurityAnswer = (security_answer) => {
    setAnswerError(_security_answer_validator(security_answer));
  };
  const resetErrors = () => {
    setEmpidError("");
    setNewPasswordError("");
    setQuestionError("");
    setAnswerError("");
  };
  const statusMessage = ()=>{
    setHeadstatus("")
  }

  const submitForm = async (event) => {
    event.preventDefault();
    resetErrors();
    validateEmpid(empid);
    validateNewPassword(new_password);
    validateSecurityQuestion(security_question);
    validateSecurityAnswer(security_answer);

    if (
      _empid_validator(empid).length > 0 ||
      _new_password_validator(new_password).length > 0 ||
      _security_question_validator(security_question).length > 0 ||
      _security_answer_validator(security_answer).length > 0
    ) {
      console.log("ERROR");
      return;
    } else {
      // const { empid, new_password, security_question, security_answer } = this.state;
      const userDetails = {
        empid,
        new_password,
        security_question,
        security_answer,
      };
      const url = "http://localhost:8002/forgot";
      const options = {
        method: "POST",
        body: JSON.stringify(userDetails),
        headers: {
          "Content-Type": "application/json",
          Accept: "application/json",
        },
      };

      try {
        const response = await fetch(url, options);
        const data = await response.json();
        if (data) {
           console.log("DataMessage",data.message)
          if (data.message=="Password updated successfully") {
            history.replace("/login");
          } else {
            // no jwt token recieved
            setHeadstatus(data.message)
            setTimeout( statusMessage,4000)
            // setNewPasswordError(data.message);
          }
        } else {
          setHeadstatus("No data recieved")
          setTimeout( statusMessage,4000)
          // setNewPasswordError("No data recieved")
        }
      } catch (error) {
        // unknown error
        setHeadstatus("Unable to contact server.");
        setTimeout( statusMessage,4000);
      }
    }
  };
  // render(){
  //   const {email, new_password,security_answer} = this.state

  return (
    
    <div className="mainDiv1">
      <div className="cardStyle1">
      <h2>{headstatus}</h2>
        <form action="" method="post" name="signupForm" id="signupForm">
          <h2 className="formTitle1">Reset your password</h2>
          <div className="inputDiv1">
            <label class="inputLabel1" for="EmpId">
              Emp Id
            </label>
            <input className="input1"
              type="number"
              id="empid"
              name="EmpId"
              value={empid}
              onChange={(event) => onChangeEmpid(event)}
              onBlur={(event) => onBlurEmpid(event)}
              required
            />
            <div>{empidError}</div>
          </div>

          <div className="inputDiv1">
            <label class="inputLabel1" for="password">
              New Password
            </label>
            <input className="input1"
              type="text"
              id="password"
              name="password"
              value={new_password}
              onChange={(event) => onChangeNewPassword(event)}
              onBlur={(event) => onBlurNewPassword(event)}
              required
            />
            <div>{newpasswordError}</div>
          </div>

          <div className="inputDiv1">
            <label className="inputLabel1" for="security_question">
              security question
            </label>
            <input className="input1"
              type="text"
              id="security_question"
              name="security question"
              value={security_question}
              onChange={(event) => onChangeSecurityQuestion(event)}
              onBlur={(event) => onBlurSecurityQuestion(event)}
            />
            <div>{security_question_err}</div>
          </div>
          <div className="inputDiv1">
            <label className="inputLabel1" for="securityanswer">
              security answer
            </label>
            <input className="input1"
              type="security_answer"
              id="security_answer"
              name="security answer"
              value={security_answer}
              onChange={(event) => onChangeSecurityAnswer(event)}
              onBlur={(event) => onBlurSecurityAnswer(event)}
            />
            <div>{security_answer_err}</div>
          </div>
        </form>
        <div className="buttonWrapper">
          <button
            type="submit"
            id="submit-Button"
            onClick={submitForm}
            className="submitButton pure-button pure-button-primary">
              {/* <Link to="/login" > Continue</Link> */}
              <span> Continue </span>
          </button>
          <button type="submit"
            id="submit-Button"
            className="submitButton pure-button pure-button-primary">
              <span><Link to="/login">BacktoLogin</Link></span>
              </button>
        </div>
      </div>
    </div>
  );
};

export default ForgotPassword;
