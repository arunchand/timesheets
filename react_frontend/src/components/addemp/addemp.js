// External Dependencies
import React, { useState } from "react";
import { Link, useHistory } from "react-router-dom";
// Internal Dependencies
import "./addemp.css";
import {
  _name_validator,
  _empid_validator,
  _password_validator,
  _date_of_joining_validator,
  _role_validator,
  _email_validator,
  _security_question_validator,
  _security_answer_validator,
  _phoneNo_validator,
} from "../../input_validators";

// Declaring Function AddEmp to add employee for Admin
function AddEmp() {
  // Declaring history object
  const history = useHistory;
  // Declaring state
  const [name, setName] = useState("");
  const [empid, setEmpid] = useState("");
  const [password, setPassword] = useState("");
  const [dateofjoining, setDateOfJoining] = useState("");
  const [role, setRole] = useState("");
  const [email, setEmail] = useState("");
  const [phoneNo, setPhoneNo] = useState("");
  const [security_question, setSecurity_question] = useState("");
  const [security_answer, setSecurity_answer] = useState("");
  const [headStatus, setHeadStatus] = useState("");
  // Declaring state for error messages
  const [nameError, setNameError] = useState("");
  const [empidError, setEmpidError] = useState("");
  const [passwordError, setPasswordError] = useState("");
  const [dateofjoiningError, setDateOfJoiningError] = useState("");
  const [roleError, setRoleError] = useState("");
  const [emailError, setEmailError] = useState("");
  const [phoneNoError, setPhoneNoError] = useState("");
  const [security_questionError, setSecurity_questionError] = useState("");
  const [security_answerError, setSecurity_answerError] = useState("");

  /**
   * Name
   * onchangeName function takes the event value
   * it will set state value and error value for Name.
   * @param {*} event
   */
  const onChangeName = (event) => {
    setName(event.target.value);
    setNameError("");
  };
  /**
   * OnBlur function takes the event value as a param
   * calls the validateName function to validate the param
   * throws error if any
   * @param {*} event
   */
  const onBlurName = (event) => {
    validateName(event.target.value);
  };
  // Validates the name and set error if any
  const validateName = (name) => {
    setNameError(_name_validator(name));
  };

  /**
   * employee ID
   * onchangeName function takes the event value
   * it will set state value and error value for Employee ID.
   * @param {*} event
   */
  const onChangeEmpid = (event) => {
    setEmpid(parseInt(event.target.value));
  };
  /**
   * OnBlur function takes the event value as a param
   * calls the validateEmpid function to validate the param
   * throws error if any
   * @param {*} event
   */
  const onBlurEmpid = (event) => {
    validateEmpid(event.target.value);
    setEmpidError("");
  };
  // Validates the Empid and set error if any
  const validateEmpid = (empid) => {
    setEmpidError(_empid_validator(empid));
  };

  /**
   * password
   * onchangeName function takes the event value
   * it will set state value and error value for Password.
   * @param {*} event
   */
  const onChangePassword = (event) => {
    setPassword(event.target.value);
    setPasswordError("");
  };
  /**
   * OnBlur function takes the event value as a param
   * calls the validatePassword function to validate the param
   * throws error if any
   * @param {*} event
   */
  const onBlurPassword = (event) => {
    validatePassword(event.target.value);
  };
  // Validates the Password and set error if any
  const validatePassword = (password) => {
    setPasswordError(_password_validator(password));
  };

  /**
   * date of joining
   * onchangeName function takes the event value
   * it will set state value and error value for date of joining.
   * @param {*} event
   */
  const onChangeDateOfJoining = (event) => {
    setDateOfJoining(event.target.value);
    setDateOfJoiningError("");
  };
  /**
   * OnBlur function takes the event value as a param
   * calls the validate function to validate the param
   * throws error if any
   * @param {*} event
   */
  const onBlurDateOfJoining = (event) => {
    validateDateOfJoining(event.target.value);
  };
  // Validates the Date of joining and set error if any
  const validateDateOfJoining = (dateofjoining) => {
    setDateOfJoiningError(_date_of_joining_validator(dateofjoining));
  };

  /**
   * Role
   * onchangeName function takes the event value
   * it will set state value and error value for Role.
   * @param {*} event
   */
  const onChangeRole = (event) => {
    setRole(event.target.value);
    setRoleError("");
  };
  /**
   * OnBlur function takes the event value as a param
   * calls the validate function to validate the param
   * throws error if any
   * @param {*} event
   */
  const onBlurRole = (event) => {
    validateRole(event.target.value);
  };
  // Validates the Role and set error if any
  const validateRole = (role) => {
    setRoleError(_role_validator(role));
  };

  /**
   * Email ID
   * onchangeName function takes the event value
   * it will set state value and error value for Email ID.
   * @param {*} event
   */
  const onChangeEmail = (event) => {
    setEmail(event.target.value);
    setEmailError("");
  };
  /**
   * OnBlur function takes the event value as a param
   * calls the validate function to validate the param
   * throws error if any
   * @param {*} event
   */
  const onBlurEmail = (event) => {
    validateEmail(event.target.value);
  };
  // Validates the email and set error if any
  const validateEmail = (email) => {
    setEmailError(_email_validator(email));
  };

  /**
   * Phone number
   * onchangeName function takes the event value
   * it will set state value and error value for Phone number.
   * @param {*} event
   */
  const onChangePhoneNo = (event) => {
    setPhoneNo(parseInt(event.target.value));
    setPhoneNoError("");
  };
  /**
   * OnBlur function takes the event value as a param
   * calls the validate function to validate the param
   * throws error if any
   * @param {*} event
   */
  const onBlurPhoneNo = (event) => {
    validatePhoneNo(event.target.value);
  };
  // Validates the phone number and set error if any
  const validatePhoneNo = (phoneNo) => {
    setPhoneNoError(_phoneNo_validator(phoneNo));
  };

  /**
   * Security question
   * onchangeName function takes the event value
   * it will set state value and error value for security question.
   * @param {*} event
   */
  const onChangeSecurityQuestion = (event) => {
    setSecurity_question(event.target.value);
    setSecurity_questionError("");
  };
  /**
   * OnBlur function takes the event value as a param
   * calls the validate function to validate the param
   * throws error if any
   * @param {*} event
   */
  const onBlurSecurityQuestion = (event) => {
    validateSecurityQuestion(event.target.value);
  };
  // Validates the Security Question and set error if any
  const validateSecurityQuestion = (security_question) => {
    setSecurity_questionError(_security_question_validator(security_question));
  };

  /**
   * security answer
   * onchangeName function takes the event value
   * it will set state value and error value for security answer.
   * @param {*} event
   */
  const onChangeSecurityAnswer = (event) => {
    setSecurity_answer(event.target.value);
    setSecurity_answerError("");
  };
  /**
   * OnBlur function takes the event value as a param
   * calls the validate function to validate the param
   * throws error if any
   * @param {*} event
   */
  const onBlurSecurityAnswer = (event) => {
    validateSecurityAnswer(event.target.value);
  };
  // Validates the Security answer and set error if any
  const validateSecurityAnswer = (security_answer) => {
    setSecurity_answerError(_security_answer_validator(security_answer));
  };
  // Decalring Reset Errors 
  const resetErrors = () => {
    setNameError("");
    setEmpidError("");
    setPasswordError("");
    setDateOfJoiningError("");
    setRoleError("");
    setEmailError("");
    setPhoneNoError("");
    setSecurity_questionError("");
    setSecurity_answerError("");
  };
  // Declaring Reset Feilds
  const resetFields = () => {
    setName("");
    setEmpid("");
    setPassword("");
    setDateOfJoining("");
    setRole("");
    setEmail("");
    setPhoneNo("");
    setSecurity_question("");
    setSecurity_answer("");
  };

  //time out function
  const clearStatus = () => {
    setHeadStatus("");
  };
  //submitting the form
  const submitForm = async (event) => {
    event.preventDefault();
    resetErrors();
    validateName(name);
    validateEmpid(empid);
    validatePassword(password);
    validateDateOfJoining(dateofjoining);
    validateRole(role);
    validateEmail(email);
    validatePhoneNo(phoneNo);
    validateSecurityQuestion(security_question);
    validateSecurityAnswer(security_answer);

    // Throws error if any feild is missing
    if (
      _name_validator(name).length > 0 ||
      _empid_validator(empid).length > 0 ||
      _password_validator(password).length > 0 ||
      _date_of_joining_validator(dateofjoining).length > 0 ||
      _role_validator(role).length > 0 ||
      _email_validator(email).length > 0 ||
      _phoneNo_validator(phoneNo).length > 0 ||
      _security_question_validator(security_question).length > 0 ||
      _security_answer_validator(security_answer).length > 0
    ) {
      console.log("ERROR");
      return;
    } else {
      const userDetails = {
        name,
        empid,
        password,
        dateofjoining,
        role,
        email,
        phoneNo,
        security_question,
        security_answer
      };
      const url = "http://localhost:8002/register";
      const options = {
        method: "POST",
        body: JSON.stringify(userDetails),
        headers: {
          "Content-Type": "application/json",
          Accept: "application/json",
          authorization:
            "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbXBpZCI6MTA3NjYsInJvbGUiOiJhZG1pbiIsImlhdCI6MTY0NjcyMDYxOH0.G7gcoRh6QyaMCp8io6_JRhgiKZ-6vZ2Q54dKg9YyJRc",
        },
      };

      try {
        const response = await fetch(url, options);
        const data = await response.json();
        if (data.message == "User Registration Successfull") {
          setHeadStatus(data.message);
          setTimeout(clearStatus, 4000);
          resetFields();
        } else {
          setHeadStatus(data.message);
          setTimeout(clearStatus, 5000);
          console.log(resetFields);
        }
      } catch (error) {
        // unknown error
        setHeadStatus("Unable to contact server.");
      }
    }
  };

  return (
    <div className="container">
      <div className="wrapper">
        <form>
          <h1 align="center">Add New Employee</h1>
          <h2 className="headstatus">{headStatus}</h2>
          <input
            type="text"
            id="name"
            name="name"
            value={name}
            onChange={(event) => onChangeName(event)}
            onBlur={(event) => onBlurName(event)}
            required
            placeholder="Full Name"
          />
          <div>{nameError}</div>
          <input
            type="number"
            id="employee"
            name="EmpId"
            value={empid}
            onChange={(event) => onChangeEmpid(event)}
            onBlur={(event) => onBlurEmpid(event)}
            required
            placeholder="Employee Id"
          />
          <div>{empidError}</div>
          <input
            type="text"
            id="role"
            name="role"
            value={role}
            onChange={(event) => onChangeRole(event)}
            onBlur={(event) => onBlurRole(event)}
            required
            placeholder="Role"
          />
          <div>{roleError}</div>
          <input
            type="email"
            id="email"
            name="Email"
            value={email}
            onChange={(event) => onChangeEmail(event)}
            onBlur={(event) => onBlurEmail(event)}
            required
            placeholder="Email"
          />
          <div>{emailError}</div>
          <input
            type="number"
            id="phoneno"
            name="PhoneNo"
            value={phoneNo}
            onChange={(event) => onChangePhoneNo(event)}
            onBlur={(event) => onBlurPhoneNo(event)}
            required
            placeholder="Phone Number"
          />
          <div>{phoneNoError}</div>
          <input
            type="password"
            id="password_"
            name="EmpId"
            value={password}
            onChange={(event) => onChangePassword(event)}
            onBlur={(event) => onBlurPassword(event)}
            required
            placeholder="Password"
          />
          <div>{passwordError}</div>
          <input
            type="text"
            id="securityques"
            name="security_question"
            value={security_question}
            onChange={(event) => onChangeSecurityQuestion(event)}
            onBlur={(event) => onBlurSecurityQuestion(event)}
            required
            placeholder="Security Question"
          />
          <div>{security_questionError}</div>
          <input
            type="password"
            id="securityans"
            name="security_answer"
            value={security_answer}
            onChange={(event) => onChangeSecurityAnswer(event)}
            onBlur={(event) => onBlurSecurityAnswer(event)}
            required
            placeholder="Security Answer"
          />
          <div>{security_answerError}</div>
          <input
            type="number"
            id="dateofjoining"
            name="dateofjoining"
            value={dateofjoining}
            onChange={(event) => onChangeDateOfJoining(event)}
            onBlur={(event) => onBlurDateOfJoining(event)}
            required
            placeholder="Date of Joining  DDMMYYYY"
          />
          <div>{dateofjoiningError}</div>
          <input
            type="submit"
            id="login-btn"
            value="Add"
            onClick={submitForm}
          />
        </form>
      </div>
    </div>
  );
}
// Exporting  AddEmp function
export default AddEmp;
