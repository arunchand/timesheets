import React, { Component } from "react";
import { Link } from "react-router-dom";
import "./index.css";

const Timesheets = () => {
  return (
    <>
      <div className="d-flex bg-info align-items-center justify-content-between py-4 px-4 rounded-top border-bottom text-white">
        <svg
          version="1.1"
          className="user-icon"
          xmlns="http://www.w3.org/2000/svg"
          xmlnsXlink="http://www.w3.org/1999/xlink"
          x="0px"
          y="0px"
          viewBox="0 0 512 512"
          style={{ enableBackground: "new 0 0 512 512" }}
          xmlSpace="preserve"
          width="75px"
          height="75px"
        >
          <path
            style={{ fill: "white" }}
            d="M379.07,234.64c-9.47-3.59-20.57-2.11-28.25,4.49c-24.07,20.7-58.2,33.57-94.83,33.74c-36.63-0.17-70.76-13.04-94.83-33.74c-7.68-6.6-18.78-8.08-28.25-4.49C70.51,258.3,10.38,378.81,58.39,451.26c38.54,58.16,122.38,55.58,197.61,55.58c75.23,0,159.08,2.58,197.61-55.58C501.62,378.81,441.49,258.3,379.07,234.64z M273.88,229.95c47.62-7.29,86.54-46.22,93.84-93.84c11.7-76.42-53.17-141.3-129.6-129.59c-47.62,7.29-86.54,46.21-93.84,93.84C132.58,176.77,197.46,241.65,273.88,229.95z"
          />
        </svg>
        <h1 className="mb-0 font-weight-bold">Timesheets</h1>
      </div>
      <div className="d-flex flex-row">
        <div className="card card1">
          {/* <button className="b1" onclick="window.location.href='ehome.html'">
        My profile
      </button>
      <button className="b1" onclick="window.location.href='timesheet.html'">
        Time sheets
      </button> */}
          {/* <button className="b1">My Requests</button>
      <button className="b1">Awards</button>
      <button className="b1">Pay Slips</button> */}
        </div>
        <div className="d-flex flex-column tables">
          <div className="d-flex flex-row">
            {/* <div className="header1 m-2">
          <h4>Work from home</h4>
        </div>
        <div className="header2 m-2">
          <h4>Holidays</h4>
        </div>
        <div className="header3 m-2">
          <h4>Leave</h4>
        </div>
        <div className="header4 m-2">
          <h4>Present days</h4>
        </div> */}
          </div>
          <div>
            <table style={{ width: "100%" }}>
              <tbody>
                <tr>
                  <th>Monday</th>
                  <th>Tuesday</th>
                  <th>Wednesday</th>
                  <th>Thursday</th>
                  <th>Friday</th>
                  <th>Saturday</th>
                  <th>Sunday</th>
                </tr>
                <tr>
                  <td>
                    <Link to="/taskpage">
                      <button className="btn3" />1
                    </Link>
                  </td>
                  <td>
                    <button className="btn3" />2
                  </td>
                  <td>
                    <button className="btn3" />3
                  </td>
                  <td>
                    <button className="btn3" />4
                  </td>
                  <td>
                    <button className="btn3" />5
                  </td>
                  <td>
                    <button className="btn3" />6
                  </td>
                  <td>
                    <button className="btn3" />7
                  </td>
                </tr>
                <tr>
                  <td>
                    <button className="btn3" />8
                  </td>
                  <td>
                    <button className="btn3" />9
                  </td>
                  <td>
                    <button className="btn3" />
                    10
                  </td>
                  <td>
                    <button className="btn3" />
                    11
                  </td>
                  <td>
                    <button className="btn3" />
                    12
                  </td>
                  <td>
                    <button className="btn3" />
                    13
                  </td>
                  <td>
                    <button className="btn3" />
                    14
                  </td>
                </tr>
                <tr>
                  <td>
                    <button className="btn3" />
                    15
                  </td>
                  <td>
                    <button className="btn3" />
                    16
                  </td>
                  <td>
                    <button className="btn3" />
                    17
                  </td>
                  <td>
                    <button className="btn3" />
                    18
                  </td>
                  <td>
                    <button className="btn3" />
                    19
                  </td>
                  <td>
                    <button className="btn3" />
                    20
                  </td>
                  <td>
                    <button className="btn3" />
                    21
                  </td>
                </tr>
                <tr>
                  <td>
                    <button className="btn3" />
                    22
                  </td>
                  <td>
                    <button className="btn3" />
                    23
                  </td>
                  <td>
                    <button className="btn3" />
                    24
                  </td>
                  <td>
                    <button className="btn3" />
                    25
                  </td>
                  <td>
                    <button className="btn3" />
                    26
                  </td>
                  <td>
                    <button className="btn3" />
                    27
                  </td>
                  <td>
                    <button className="btn3" />
                    28
                  </td>
                </tr>
                <tr>
                  <td>
                    <button className="btn3" />
                    29
                  </td>
                  <td>
                    <button className="btn3" />
                    30
                  </td>
                  <td />
                  <td />
                  <td />
                  <td />
                  <td />
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </>
  );
};

export default Timesheets;
