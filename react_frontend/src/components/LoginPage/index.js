import { Link, useHistory } from "react-router-dom";
import { useState, useContext } from "react";
import { _empid_validator, _password_validator } from "../../input_validators";
import "./index.css";
import Cookies from "js-cookie";

function LoginPage() {
  const history = useHistory();

  // const [role, setRole] = useState("")
  const [empid, setEmpid] = useState("");
  const [password, setPassword] = useState("");
  const [empidError, setEmpidError] = useState("");
  const [passwordError, setPasswordError] = useState("");

  const onChangeEmpid = (event) => {
    setEmpid(parseInt(event.target.value));
  };

  const onBlurEmpid = (event) => {
    validateEmpid(event.target.value);
    setEmpidError("");
    setPasswordError("");
  };

  const validateEmpid = (empid) => {
    setEmpidError(_empid_validator(empid));
  };

  const onChangePassword = (event) => {
    setPassword(event.target.value);
    setPasswordError("");
  };

  const onBlurPassword = (event) => {
    validatePassword(event.target.value);
  };
  const validatePassword = (password) => {
    setPasswordError(_password_validator(password));
  };

  const resetErrors = () => {
    setEmpidError("");
    setPasswordError("");
  };
  const submitForm = async (event) => {
    event.preventDefault();
    resetErrors();
    validateEmpid(empid);
    validatePassword(password);

    const userDetails = { empid, password };
    console.log(_password_validator(password));
    if (
      _empid_validator(empid).length > 0 ||
      _password_validator(password).length > 0
    ) {
      console.log("from submitted");
      return;
    } else {
      const url = "http://localhost:8002/login";
      const options = {
        method: "POST",
        body: JSON.stringify(userDetails),
        headers: {
          "Content-Type": "application/json",
          Accept: "application/json",
        },
      };
      try {
        const response = await fetch(url, options);
        const data = await response.json();
        console.log("data :", data);
        if (response.status === 200) {
          if (data.jwt_token) {
            Cookies.set("jwt_token", data.jwt_token, {
              expires: 30,
            });
            history.push("/emphome");
          } else {
            setPasswordError("No JWT Token recieved");
          }
        } else {
          setPasswordError(data.status);
        }
      } catch (error) {
        setPasswordError("Unable to contact server.");
      }
    }
  };

  return (
    <>
      <div class="vid-container">
        <div class="box">
          <h1>Login</h1>

            {/* <input type="text" value={role} onChange={event => setRole(event.target.value)} placeholder="Role"/> */}
            <input
              type="number"
              value={empid}
              onChange={(event) => onChangeEmpid(event)}
              onBlur={(event)=>onBlurEmpid(event)}
              placeholder="Employee Id"
            />
            <div>{empidError}</div>
            <input
              type="text"
              value={password}
              onChange={(event) =>onChangePassword(event)}
              onBlur={(event)=>onBlurPassword(event)}
              placeholder="Password"
            />
            <div>{passwordError}</div>
              <button type="submit" onClick={submitForm}>
                Login
              </button>
            <Link to="/forgot">
              <a href="#" className="para1">
                Forgot Password
              </a>
            </Link>
          </div>
        </div>
    </>
  );
}

export default LoginPage;
