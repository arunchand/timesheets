import React, { useState, useEffect } from "react";
import { Link, useHistory } from "react-router-dom";
import Cookies from "js-cookie";
import "./index.css";

function Profile() {
  const [data, setData] = useState({});
  const [responseError, setResponseError] = useState("");

  const history = useHistory();
  const onClickSaveChanges = () => {
    history.push("/emphome");
  };
  useEffect(() => {
    async function fetch_data() {
      const optoins = {
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${Cookies.get("jwt_token")}`,
        },
      };
      console.log(`Token key ${Cookies.get("jwt_token")}`);
      try {
        const response = await fetch(
          "http://localhost:8002/get/Employee_details",
          optoins
        );
        const data = await response.json();
        setData(data);
        console.log("data from database", data);
      } catch {
        setResponseError("data not found");
      }
    }
    fetch_data();
  }, []);
  return (
    <>
      <div class="profile-container1">
        <div class="profile-card1">
          <h3 class="card-heading1">Profile Picture</h3>
          <div>
            <img
              class="img-account-profile1"
              src="https://i.ibb.co/yNGW4gg/avatar.png"
              alt=""
            />
          </div>
          <div>
            <button class="btn1" type="button">
              Upload new image
            </button>
          </div>
        </div>
        <div class="profile-details1">
          <h3 class="card-heading1">Details</h3>
          <div id="profile-data">
            <form id="inner-data">
              <div className="row">
                <div class="input-container1">
                  <label class="profile-label" for="inputUsername">
                    Employee Id:
                  </label>
                  <p class="form-input1">{data.empid}</p>
                </div>

                <div class="input-container1">
                  <label class="profile-label" for="inputFirstName">
                    Employee Name:
                  </label>
                  <p class="form-input1">{data.name}</p>
                </div>

                <div class="input-container1">
                  <label class="profile-label" for="inputFirstName">
                    Role:
                  </label>
                  <p class="form-input1">{data.role}</p>
                </div>

                <div class="input-container1">
                  <label class="profile-label" for="inputFirstName">
                    DateofJoining:
                  </label>
                  <p class="form-input1">{data.dateofjoining}</p>
                </div>

                <div class="input-container1">
                  <label class="profile-label" for="inputFirstName">
                    EMail:
                  </label>
                  <p class="form-input1">{data.email}</p>
                </div>

                <div class="input-container1">
                  <label class="profile-label" for="inputFirstName">
                    Phone Number:
                  </label>
                  <p class="form-input1">{data.phoneNo}</p>
                </div>
              </div>
              <div>{responseError}</div>

              <button class="btn1" onClick={onClickSaveChanges} type="button">
                Edit Profile
              </button>
            </form>
          </div>
        </div>
      </div>
    </>
  );
}

export default Profile;
