import React, { useState } from "react";
import { Link, useHistory } from "react-router-dom";
import "./taskPage.css";
import { _taskname_validator, _date_validator,
_hours_validator, _description_validator } from "../../input_validators";

const TaskPage = () => {
  const [date, setDate]=useState("")
  const [task_name, setTaskName]=useState("")
  const [hours, setHours]=useState("")
  const [description, setDescription]=useState("")
  const [task_name_err, setTaskNameError ]=useState("")
  const [hours_err, setHoursError]=useState("")
  const [description_err, setDescriptionError]=useState("")
  const [date_err, setDateError]=useState("")

  const history = useHistory();

  const onChangeDate = (event) => {
    setDate(event.target.value);
    setDateError("");
  };
  const onBlurDate = (event) => {
    validateDate(event.target.value);
    //setNewPasswordError("")
  };
  const validateDate = (date) => {
    setDateError(_date_validator(date));
  };


  const onChangeTaskname = (event) => {
    setTaskName(event.target.value);
    setTaskNameError("");
  };
  const onBlurTaskname = (event) => {
    validateTaskName(event.target.value);
    //setNewPasswordError("")
  };
  const validateTaskName = (task_name) => {
    setTaskNameError(_taskname_validator(task_name));
  };

  
  const onChangeHours = (event) => {
    setHours(event.target.value);
    setHoursError("");
  };
  const onBlurHours = (event) => {
    validateHours(event.target.value);
    //setNewPasswordError("")
  };
  const validateHours = (hours) => {
    setTaskNameError(_hours_validator(hours));
  };

  
  const onChangeDescription = (event) => {
    setDescription(event.target.value);
    setDescriptionError("");
  };
  const onBlurDescription = (event) => {
    validateDescription(event.target.value);
    //setNewPasswordError("")
  };
  const validateDescription = (description) => {
    setTaskNameError(_hours_validator(description));
  };
  const resetErrors = () => {
    setDateError("");
    setTaskNameError("");
    setHoursError("");
    setDescriptionError("");
  };
 
  const submitForm = async (event) => {
    event.preventDefault();
    resetErrors();
    validateDate(date);
    validateTaskName(task_name);
    validateHours(hours);
    validateDescription(description);

    if (
      _date_validator(date).length > 0 ||
      _taskname_validator(task_name).length > 0 ||
      _hours_validator(hours).length > 0 ||
      _description_validator(description).length > 0
    ) {
      console.log("ERROR");
      return;
    } else {
      const userDetails = {
        date,
        task_name,
        hours,
        description,
      };
      const url = "http://localhost:8002/daily_task";
      const options = {
        method: "POST",
        body: JSON.stringify(userDetails),
        headers: {
          "Content-Type": "application/json",
          Accept: "application/json",
        },
      };

      try {
        const response = await fetch(url, options);
        //console.log(response.status)
        const data = await response.json();
        console.log("data :", data);

        if (data.status === 200) {
          alert("task updated succesfully");
          //console.log("hello")
          if (data) {
            history.push("/login");
          } else {
            // no jwt token recieved
            setTaskNameError("No data recieved");
          }
        } else {
          setTaskNameError(data.status);
        }
      } catch (error) {
        // unknown error
        setTaskNameError("Unable to contact server.");
      }
    }
  };


  return (
    <div className="taskpage-container">
      <h2>Update your tasks </h2>
      <form>
        <div id="taskpage-header">
        <label for="date">
            Date
            <input type="text" 
            value={date}
            onChange={(event) => onChangeDate(event)}
            onBlur={(event) => onBlurDate(event)} />
            <div>{date_err}</div>
          </label>
          <label for="task">
            Task
            <input type="text" value={task_name}
            onChange={(event) => onChangeTaskname(event)}
            onBlur={(event) => onBlurTaskname(event)}/>
            <div>{task_name_err}</div>
          </label>
          <label for="from-time">
            Hours
            <input type="text" value={hours}
            onChange={(event) => onChangeHours(event)}
            onBlur={(event) => onBlurHours(event)}/>
            <div>{hours_err}</div>
          </label>
          <label for="to-time">
            Description
            <input type="text" value={description}
            onChange={(event) => onChangeDescription(event)}
            onBlur={(event) => onBlurDescription(event)}/>
            <div>{description_err}</div>
          </label>
        </div>
        <button id="addtask-btn" onClick={submitForm}>Add Task</button>
      </form>
      <div className="data-container">
        <div className="display-content">
        <div className="date-heading">
            <h3>Date</h3>
          </div>
          <div className="task-heading">
            <h3>Task Name</h3>
          </div>
          <div className="start-heading">
            <h3>Hours</h3>
          </div>
          <div className="end-heading">
            <h3>Description</h3>
          </div>
        </div>

        <div className="data">
          {/* <div calss="tasks-data">
            <h4>Meeting</h4>
        </div>
         <div calss="hours-data">
            <h4>2</h4>
        </div>
        <div class="description">
            <h4>description</h4>
        </div>  */}
        </div>
      </div>
      <Link to="/timesheets">
        <button id="submit-btn" onClick={submitForm}>Submit</button>
      </Link>
    </div>
  );
};

export default TaskPage;
