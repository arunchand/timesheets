import { useState,useEffect } from "react";
import { Link ,Redirect} from "react-router-dom";
import Cookies from "js-cookie";
import "./index.css"


const EmpHome=({history})=>{

   const [data,setData] = useState({})
   const [responseError,setResponseError] = useState("")

   const onClickLogout = ()=>{
      console.log("logout");
      Cookies.remove('jwt_token')
      history.push('/login');
    }
    useEffect(()=>{
      async function fetch_data(){
           const optoins = {
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${Cookies.get("jwt_token")}`,
        },
      };
        console.log(`Token key ${Cookies.get("jwt_token")}`)
        try {
          const response = await fetch("http://localhost:8002/get/Employee_details",optoins);
          const data = await response.json();
          setData(data)
          console.log("data from database",data);
        } catch {
          setResponseError("data not found");
        }
      }
      fetch_data();
    }
    ,[])
    

    return (
      <>
        <div className="header-container">
          <div className="test">
            <input type="checkbox" className="menu-toggle" id="menu-toggle" />
            <div className="mobile-bar">
              <label htmlFor="menu-toggle" className="menu-icon">
                <span />
              </label>
            </div>
            <div className="header">
              <nav>
                <ul>
                  <li>
                    <a href="#">{data.name}</a>
                  </li>
                  <li>
                    <a href="#">{`logged in as ${data.role}`}</a>
                  </li>
                  <li>
                    <a onClick={onClickLogout}>Logout</a>
                  </li>
                </ul>
              </nav>
            </div>
            <a className="navbar-brand" style={{ padding: 0 }}>
              <img src="http://www.innominds.com/hubfs/logo%20-03.png" />
            </a>
          </div>
        </div>
        <div className="sidebar-container">
          <div className="sidebar">
            <div className="active sidebar-header">
              <svg
                stroke="currentColor"
                fill="currentColor"
                strokeWidth={0}
                viewBox="0 0 24 24"
                height={24}
                width={24}
                xmlns="http://www.w3.org/2000/svg"
              >
                <path fill="none" d="M0 0h24v24H0V0z" />
                <path d="M18 4H6c-1.1 0-2 .9-2 2v12c0 1.1.9 2 2 2h12c1.1 0 2-.9 2-2V6c0-1.1-.9-2-2-2zm0 14H6V6h12v12z" />
              </svg>
              <span>Company</span>
            </div>
            <div className="below">
              <div>
                <div className="sidebar-body">
                  <Link to="/profile">
                    <div>
                      <img
                        src="https://img.icons8.com/ios-glyphs/60/000000/test-account.png"
                        className="icon-img"
                      />
                      <span>Profile</span>
                    </div>
                  </Link>

                  <Link to="/timesheets">
                    <div>
                      <img
                        src="https://img.icons8.com/ios-filled/60/000000/property-time.png"
                        className="icon-img"
                      />
                      <span>Timesheet</span>
                    </div>
                  </Link>
                  <div>
                    <h1>{responseError}</h1>
                  </div>
                  {/* <a href="#">
              <div>
                <img
                  src="https://img.icons8.com/ios-glyphs/60/000000/leave.png"
                  className="icon-img"
                />
                <span>Requests</span>
              </div>
            </a> */}
                  {/* <a href="#">
              <div>
                <img
                  src="https://img.icons8.com/ios-filled/60/000000/purchase-order.png"
                  className="icon-img"
                />
                <span>Payslips</span>
              </div>
            </a> */}
                  {/* <a href="#">
              <div>
                <img
                  src="https://img.icons8.com/ios-glyphs/60/000000/training.png"
                  className="icon-img"
                />
                <span>Training Sessions</span>
              </div>
            </a> */}
                </div>
              </div>
            </div>
          </div>
          <div className="just">
            <div>
              <h1>Welcome {data.name ? data.name.toUpperCase() : ""}</h1>
              <br />
              <h2>Our Evolution</h2>
              <p>
                Innominds is an AI-first, platform-led digital transformation
                and full cycle product engineering services company
                headquartered in San Jose, CA. Innominds powers the Digital Next
                initiatives of global enterprises, software product companies,
                OEMs and ODMs with integrated expertise in devices & embedded
                engineering, software apps & product engineering, analytics &
                data engineering, quality engineering, and cloud & devops,
                security. It works with ISVs to build next-generation products,
                SaaSify, transform total experience, and add cognitive analytics
                to applications.
              </p>
              <img
                id="home-img"
                alt="About Innominds Vison &amp; Evolution"
                title="Our Evolution"
                src="https://www.innominds.com/hs-fs/hubfs/Innominds-201612/img/IM-inner-pages/IM-about/Company-Evolution-New.jpg?width=1547&amp;name=Company-Evolution-New.jpg"
              />
            </div>
          </div>
        </div>
      </>
    );
  }


export default EmpHome
