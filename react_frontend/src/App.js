import "./App.css";
import LoginPage from "./components/LoginPage/index.js";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Routes,
} from "react-router-dom";
import ProtectedRoute from "./components/ProtectedRoute/index";
import Profile from "./components/Profile/index.js";
import EmpHome from "./components/EmpHome/index.js";
import Admin from "./components/AdminLogin/index.js";
import HR from "./components/HRLogin/index.js";
import AddEmp from "./components/addemp/addemp";
import ForgotPassword from "./components/ForgotPage/index.js";
import Timesheets from "./components/Timesheets/index.js";
import EmpListHr from "./components/empList/empListHr";
import EmpListAdmin from "./components/empList/empListAdmin";
import TaskPage from "./components/taskPage/taskPage";

function App() {
  return (
    <div className="App">
      <Router>
        <Switch>
          <Route path="/login" component={LoginPage} />
          <Route path="/forgot" component={ForgotPassword} />
          <ProtectedRoute path="/timesheets" component={Timesheets} />
          <ProtectedRoute path="/profile" component={Profile} />
          <Route path="/addemp" component={AddEmp} />
          <ProtectedRoute path="/emplisthr" component={EmpListHr} />
          <ProtectedRoute path="/emplistadmin" component={EmpListAdmin} />
          <ProtectedRoute path="/emphome" component={EmpHome} />
          <ProtectedRoute path="/admin" component={Admin} />
          <ProtectedRoute path="/taskpage" component={TaskPage} />
          <ProtectedRoute path="/hr" component={HR} />
        </Switch>
      </Router>
    </div>
  );
}

export default App;
